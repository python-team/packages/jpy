#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

<<<<<<< HEAD
python -m pip install -r .github/env/Linux/requirements.txt
python setup.py bdist_wheel --dist-dir dist.linux
# Note: auditwheel only works with a single file argument - we are relying on finding exactly one wheel
python .github/env/Linux/auditwheel-keep-libjvm.py \
  repair \
  --plat manylinux_2_17_x86_64 \
  --only-plat \
=======
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

python -m pip install -r "${__dir}/requirements.txt"

# Can add --build-number <x> if necessary
python setup.py bdist_wheel --dist-dir dist.linux

# Note: auditwheel only works with a single file argument - we are relying on finding exactly one wheel
auditwheel \
  repair \
  --plat "manylinux_2_17_$(arch)" \
  --only-plat \
  --exclude libjvm.so \
>>>>>>> upstream/0.17.0
  --wheel-dir dist/ \
  dist.linux/*
